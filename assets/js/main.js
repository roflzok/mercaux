window.location.href.replace(/#.*/,'');

$(document).ready(function(){

	// Generic click bind for modal -- for future use
	$(".open_modal").click(function() {
		$("#myModal").reveal();
	});

    
    // Parallax effect
    $window = $(window);

    $('section[data-type="background"]').each(function(){
    var $bgobj = $(this); // assigning the object
                    
      $(window).scroll(function() {
                    
        // Scroll the background at var speed
        // the yPos is a negative value because we're scrolling it UP!                              
        var yPos = -($(window).scrollTop() / $bgobj.data('speed')); 
        
        // Put together our final background position
        var coords = '50% '+ yPos + 'px';

        // Move the background
        $bgobj.css({ backgroundPosition: coords });

        }); // window scroll Ends
	});    

	document.createElement("section");

    $(".philosophy1 p").fitText(2, { minFontSize: '30px', maxFontSize: '42px' });
    $(".philosophy2 p").fitText(2, { minFontSize: '24px', maxFontSize: '36px' });
    $("#prospectiveCustomers").fitText(2, { minFontSize: '30px', maxFontSize: '48px' });


    $("#toTop").scrollToTop(1000);

    $('#appScreens').orbit({
        animation: 'horizontal-push',                  // fade, horizontal-slide, vertical-slide, horizontal-push
        animationSpeed: 800,                // how fast animtions are
        timer: true,            // true or false to have the timer
        advanceSpeed: 4000,         // if timer is enabled, time between transitions 
        pauseOnHover: false,        // if you hover pauses the slider
        startClockOnMouseOut: false,    // if clock should start on MouseOut
        startClockOnMouseOutAfter: 1000,    // how long after MouseOut should the timer start again
        directionalNav: true,       // manual advancing directional navs
        captions: false,             // do you want captions?
        captionAnimation: 'fade',       // fade, slideOpen, none
        captionAnimationSpeed: 800,     // if so how quickly should they animate in
        bullets: false,             // true or false to activate the bullet navigation
        bulletThumbs: false,        // thumbnails for the bullets
        bulletThumbLocation: '',        // location from this file where thumbs will be  
        fluid: true,
        start: function(){
            $('#appScreens img').css({"visibilty" : visible});
        },
    });

//console.log('HEY DREW, LOOK: the Javascript is working!!');

}); 
